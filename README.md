# Brid iframe embed code for Facebook Instant Articles

Maximize you ad revenue by sharing your domain url as an FN instnt articles embed code.

# Minimal setup

<figure class="op-interactive"> 
<iframe src="//yourdomain.com/?id=PLAYER_ID&video=VIDOE_ID&partner_id=PARTNER_ID" allowfullscreen width="480" height="270" frameborder="0" ></iframe> 
</figure>


# Autoplay

<figure class="op-interactive"> 
<iframe src="//yourdomain.com/?id=PLAYER_ID&video=VIDOE_ID&partner_id=PARTNER_ID&autoplay=1" allowfullscreen width="480" height="270" frameborder="0" ></iframe> 
</figure>